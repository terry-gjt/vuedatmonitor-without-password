/**
 * @Description:
 * @author qianlong
 * @date $
 */

import axiosservice from '@/utils/CommonData.js'
// import axios from 'axios'

export function addAlert(data) {
  return axiosservice({
    url: '/createalert',
    method: 'get',
    params: {
      ...data
    }
  })
}

export function getAlertMessage(alertname) {
  return axiosservice({
    url: '/alert',
    method: 'get',
    params: {
      alertname
    }
  })
}

export function getMetricList() {
  return axiosservice({
    url: '/api/metrics',
    method: 'get'
  })
}
// export function getMetricList() {
//   axios.get('/api/metrics')
//     .then((res) => {
//       if (res.status == 200) {
//         return res
//         // console.log(this.metric_list.length)
//       }
//     })
//     .catch((error) => {
//       console.log(error)
//       this.$message.error('获取metrics数据失败!')
//     })
// }

export function getAlertNameList() {
  return axiosservice({
    url: '/getalertname',
    method: 'get'
  })
  // axios.get('/getalertname')
  //   .then((res) => {
  //     if (res.status == 200) {
  //       return res
  //       // console.log(this.metric_list.length)
  //     }
  //   })
  //   .catch((error) => {
  //     console.log(error)
  //     this.$message.error('获取alert数据失败!')
  //   })
}

export function deleteAlert(alertName) {
  // axios.get('/dropalert?alertname=' + alertName)
  //   .then((res) => {
  //     if (res.status == 200) {
  //       return res
  //     }
  //   })
  //   .catch((error) => {
  //     console.log(error)
  //     this.$message.error('删除alert数据失败!')
  //   })
  return axiosservice({
    url: '/dropalert',
    method: 'get',
    params: { alertName }
  })
}

export function getAlertList() {
  // axios.get('/getalert')
  //   .then((res) => {
  //     if (res.status == 200) {
  //       return res
  //     }
  //   })
  //   .catch((error) => {
  //     console.log(error)
  //     this.$message.error('获取alert数据失败!')
  //   })
  return axiosservice({
    url: '/getalert',
    method: 'get'
  })
}

