/**
 * @Description:
 * @author qianlong
 * @date $
 */
import request from '@/utils/CommonData'

export function editSettings(data) {
  return request({
    url: '/confalter',
    method: 'get',
    params: {
      ...data
    }
  })
}
