/**
 * @Description: 登入api的进一步封装
 * @author qianlong
 * @date $
 */
import request from '@/utils/request.js'

export function getImgUrl() {
  return request({
    url: '',
    method: 'get'
  })
}

export function login(data) {
  return request({
    url: '/login',
    method: 'get',
    params: {
      ...data
    }
  })
}

export function updatePassword(data) {
  console.log(data)
  return request({
    url: '/changepassword',
    method: 'get',
    params: {
      ...data
    }
  })
}

