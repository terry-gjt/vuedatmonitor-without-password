import request from '@/utils/request'
import CommonData from '@/utils/CommonData'
// export function login(data) {
//   return request({
//     url: '/vue-admin-template/user/login',
//     method: 'post',
//     data
//   })
// }
export function login(data) {
  return CommonData({
    url: '/login',
    method: 'get',
    params: {
      ...data
    }
  })
}
export function updatePassword(data) {
  return CommonData({
    url: '/changepassword',
    method: 'get',
    params: {
      ...data
    }
  })
}

export function getInfo(token) {
  return request({
    url: '/vue-admin-template/user/info',
    method: 'get',
    params: { token }
  })
}

export function logout() {
  return request({
    url: '/vue-admin-template/user/logout',
    method: 'post'
  })
}

