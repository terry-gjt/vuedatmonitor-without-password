import { login, logout, getInfo, updatePassword } from '@/api/user'
import { getToken, setToken, removeToken } from '@/utils/auth'
import { resetRouter } from '@/router'
import { Message } from 'element-ui'

const getDefaultState = () => {
  return {
    token: getToken(),
    name: '',
    avatar: ''
  }
}

const state = getDefaultState()

const mutations = {
  RESET_STATE: (state) => {
    Object.assign(state, getDefaultState())
  },
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_NAME: (state, name) => {
    state.name = name
  },
  SET_AVATAR: (state, avatar) => {
    state.avatar = avatar
  }
}

const actions = {
  // user login
  login({ commit }, userInfo) {
    const { username, password, verifycode } = userInfo
    return new Promise((resolve, reject) => {
      // 跳过登录验证
      setToken('admin-token')
      resolve()
      login({ username: username.trim(), password: password, verifycode: verifycode }).then(response => {
        // const { data } = response
        // commit('SET_TOKEN', data.token)
        // setToken(data.token)
        const res = response.data
        if (res.code == 200) {
          commit('SET_TOKEN', 'admin-token') // 添加admin-token
          setToken('admin-token')
          Message({
            message: '登录成功',
            type: 'success',
            duration: 3 * 1000
          })
          resolve()
        } else if (res.code == 205) {
          Message({
            message: '用户名或密码错误',
            type: 'error',
            duration: 3 * 1000
          })
          reject(new Error('用户名或密码错误'))
        } else if (res.code == 206) {
          Message({
            message: '验证码错误',
            type: 'error',
            duration: 3 * 1000
          })
          reject(new Error('验证码错误'))
        }
        // resolve(res)
      }).catch(error => {
        console.log('store/login', error)
        reject(error)
      })
    })
  },

  // get user info
  getInfo({ commit, state }) {
    return new Promise((resolve, reject) => {
      getInfo(state.token).then(response => {
        const { data } = response

        if (!data) {
          return reject('获取用户信息失败')
        }

        const { name, avatar } = data

        commit('SET_NAME', name)
        commit('SET_AVATAR', avatar)
        resolve(data)
      }).catch(error => {
        reject(error)
      })
    })
  },
  // 修改密码
  updatePassword({ commit }, passwordInfo) {
    const { oldpassword, newpassword } = passwordInfo
    return new Promise((resolve, reject) => {
      updatePassword({ oldpassword: oldpassword, newpassword: newpassword }).then(response => {
        removeToken() // must remove  token  first
        resetRouter()
        commit('RESET_STATE')
        resolve()
      }).catch(error => {
        console.log('修改密码', error)
        reject(error)
      })
    })
  },

  // user logout
  logout({ commit, state }) {
    return new Promise((resolve, reject) => {
      logout(state.token).then(() => {
        removeToken() // must remove  token  first
        resetRouter()
        commit('RESET_STATE')
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  // remove token
  resetToken({ commit }) {
    return new Promise(resolve => {
      removeToken() // must remove  token  first
      commit('RESET_STATE')
      resolve()
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

