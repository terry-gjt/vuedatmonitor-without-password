/**
 * @Description:
 * @author qianlong
 * @date $
 */

import { Message } from 'element-ui'

export function errorMessage(data) {
  Message({
    message: data,
    type: 'error',
    duration: 2000
  })
}
