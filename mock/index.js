const Mock = require('mockjs')
const { param2Obj } = require('./utils')

const user = require('./user')
const table = require('./table')
const TimeSeriesData = require('./TimeSeriesData')

const mocks = [
  ...user,
  ...table,
  ...TimeSeriesData
]

// for front mock
// please use it cautiously, it will redefine XMLHttpRequest,
// which will cause many of your third-party libraries to be invalidated(like progress event).
function mockXHR() {
  // mock patch
  // https://github.com/nuysoft/Mock/issues/300
  Mock.XHR.prototype.proxy_send = Mock.XHR.prototype.send
  Mock.XHR.prototype.send = function() {
    if (this.custom.xhr) {
      this.custom.xhr.withCredentials = this.withCredentials || false

      if (this.responseType) {
        this.custom.xhr.responseType = this.responseType
      }
    }
    this.proxy_send(...arguments)
  }

  function XHR2ExpressReqWrap(respond) {
    return function(options) {
      let result = null
      if (respond instanceof Function) {
        const { body, type, url } = options
        // https://expressjs.com/en/4x/api.html#req
        result = respond({
          method: type,
          body: JSON.parse(body),
          query: param2Obj(url)
        })
      } else {
        result = respond
      }
      return Mock.mock(result)
    }
  }

  for (const i of mocks) {
    Mock.mock(new RegExp(i.url), i.type || 'get', XHR2ExpressReqWrap(i.response))
  }
  // const demodata = Mock.mock({
  //   'data|6': [ // 生成6条数据 数组
  //     {
  //       'shopId|+1': 1, // 生成商品id，自增1
  //       'shopMsg': '@ctitle(10)', // 生成商品信息，长度为10个汉字
  //       'shopName': '@cname', // 生成商品名 ， 都是中国人的名字
  //       'shopTel': /^1(5|3|7|8)[0-9]{9}$/, // 生成随机电话号
  //       'shopAddress': '@county(true)', // 随机生成地址
  //       'shopStar|1-5': '★', // 随机生成1-5个星星
  //       'salesVolume|30-1000': 30, // 随机生成商品价格 在30-1000之间
  //       'shopLogo': "@Image('100x40','#c33', '#ffffff','小北鼻')", // 生成随机图片，大小/背景色/字体颜色/文字信息
  //       'food|2': [ // 每个商品中再随机生成2个food
  //         {
  //           'foodName': '@cname', // food的名字
  //           'foodPic': "@Image('100x40','#c33', '#ffffff','小可爱')", // 生成随机图片，大小/背景色/字体颜色/文字信息
  //           'foodPrice|1-100': 20, // 生成1-100的随机数
  //           'aname|2': [
  //             {
  //               'aname': '@cname',
  //               'aprice|30-60': 20
  //             }
  //           ]
  //         }
  //       ]
  //     }
  //   ]
  // })
  var basetimestamp = Date.parse(new Date())

  var writespeeddata = Mock.mock({
    'items|40': [{
      'timestamp|+1000': basetimestamp,
      'random|500-1000': 500, // 时间戳毫秒随机生成
      'writespeed': '@float(0, 1000, 1, 2)' // 0-1000小数,1-3位小数位
    }]
  })
  basetimestamp = basetimestamp + 40000
  Mock.mock(/writespeed/, 'get', () => { // 三个参数。第一个：路径，第二个：请求方式post/get，第三个：回调，返回值
    // console.log(writespeeddata2)
    writespeeddata.items = writespeeddata.items.slice(5)// 删去前5个
    var temp = Mock.mock({
      'items|5': [{
        'timestamp|+1000': basetimestamp,
        'random|500-1000': 500, // 时间戳毫秒随机生成
        'writespeed': '@float(0, 1000, 1, 2)' // 0-1000小数,1-3位小数位
      }]
    })
    basetimestamp = basetimestamp + 5000
    temp = temp.items
    for (var ttt in temp) {
      writespeeddata.items.push(temp[ttt])
    }
    console.log(writespeeddata.items.length)
    var writespeeddata2 = {}
    for (var tt in writespeeddata.items) {
      writespeeddata2[writespeeddata.items[tt].timestamp + writespeeddata.items[tt].random] = writespeeddata.items[tt].writespeed
    }
    return { data: writespeeddata2 }
  })
  // Mock.mock('/verifycode', 'post', () => {
  //   // 三个参数。第一个：路径，第二个：请求方式post/get，第三个：回调，返回值
  //   // const Result = {
  //   //   token: Random.string(32),
  //   //   image: Random.dataImage('200x100', 'p66n9')
  //   // }
  //   return { data: '666' }
  // })
}

module.exports = {
  mocks,
  mockXHR
}

