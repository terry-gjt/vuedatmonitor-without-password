const Mock = require('mockjs')

const GrafanaSettingsdata = Mock.mock({
  'connectionsnumber|15-500': 15,
  'writedatamessagenumber|15-500': 15,
  'thermaldatanumber|15-500': 15,
  'writespeedtime|15-500': 15,
  'compactNum|15-500': 15,
  'useasyncclient|15-500': 15,
  'usecompact|15-500': 15,
  'writeInterval|15-500': 15,
  'alertmessagenumber|15-500': 15,
  'email_host|15-500': 15,
  'from_username|15-500': 15,
  'from_authorization|15-500': 15,
  'email_to|15-500': 15
})

module.exports = [
  {
    url: '/presentvalue',
    type: 'get',
    response: config => {
      return GrafanaSettingsdata
    }
  }
]
